## Experimental chrome extension for creating KanColle modifications

Please note that https://poi.io/ viewer also have tools for modding. If you only need to replace textures and SFX, checkout it first.

### Requirements
* latest Chrome browser

### Usage
* [dowload](https://github.com/0x384c0/KCSModLoader/releases/download/backup/src.zip) and extract latest release
* go to `chrome://extensions`
* Enable Developer Mode by clicking the toggle switch next to Developer mode.
* Click the LOAD UNPACKED button and select the unpacked extension directory.
* load game

### Replace ingame images or sounds
* place assets, you want replace, in to `resources/default/kcs2` (for example `resources/default/kcs2/img/title/title_main.png` to change logo in start screen)
* load game

### TODO
* Update background scripts to adapt to the service worker execution context.
* KCSResourceOverride.js Migrate request modification logic to chrome.declarativeNetRequest rules.
* customize all CLT ri, CV Wo and CA Chi textures
* customize specal attacks ( PhaseNelsonTouch, PhaseNagatoAttack, PhaseMutsuAttack, PhaseColoradoAttack, etc...)
* customize all torpedo attack (with CutIns)
* customize PhaseAttackBakurai (ASW)
* customize PhaseAttackRocket (WG-42)
* customize Support fleet attack
* customize LBAS attack
* customize CV attack (topedo and dive bombers)
* customize screen shake on big explosions
* customize nuke attack (for bosses with high damage)
* customize aerial battle
* customize plane crash animations
* customize AA atacks
* fix perfomance issues in virtual machine
* auto disable on errors
* add Travis CI scripts, git flow, Unit tests, Docs 
